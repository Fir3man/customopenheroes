CREATE TABLE `accounts` (
  `accountID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(13) NOT NULL,
  `password` char(255) NOT NULL,
  `flags` int(11) DEFAULT NULL,
  PRIMARY KEY (`accountID`),
  UNIQUE KEY `uniqueuser` (`username`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

CREATE TABLE `characters` (
  `CharacterID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `charname` char(16) NOT NULL,
  `charClass` int(11) NOT NULL,
  `faction` int(11) DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `maxHP` int(11) NOT NULL,
  `currentHP` int(11) NOT NULL,
  `maxMana` int(11) NOT NULL,
  `currentMana` int(11) NOT NULL,
  `maxStamina` int(11) NOT NULL,
  `currentStamina` int(11) NOT NULL,
  `attack` int(11) NOT NULL,
  `defence` int(11) NOT NULL,
  `fame` int(11) NOT NULL,
  `flags` int(11) DEFAULT NULL,
  `locationX` int(11) NOT NULL,
  `locationY` int(11) NOT NULL,
  `map` int(11) NOT NULL,
  `intelligence` int(11) NOT NULL,
  `vitality` int(11) NOT NULL,
  `agility` int(11) NOT NULL,
  `strength` int(11) NOT NULL,
  `dexterity` int(11) NOT NULL,
  `statpoints` int(11) NOT NULL,
  `skillpoints` int(11) NOT NULL,
  `ownerID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`CharacterID`),
  UNIQUE KEY `uniquename` (`charname`) USING HASH,
  KEY `owner` (`ownerID`),
  CONSTRAINT `owner` FOREIGN KEY (`ownerID`) REFERENCES `accounts` (`accountID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=ascii;

