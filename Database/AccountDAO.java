package Database;

import java.sql.ResultSet;
import java.sql.SQLException;

import Player.Player;


public class AccountDAO {
	/*
	 * Use this for authentication
	 * Return: Player instance if auth successful, null if failed
	 */
	public static Player authenticate(String username, String pass) {
		try {
			ResultSet rs = Queries.auth(new SQLconnection().getConnection(), username, pass).executeQuery();
			if(rs.next()) {
				return new Player(rs.getInt(1));
			} else {
				return null;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
