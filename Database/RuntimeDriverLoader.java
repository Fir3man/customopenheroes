package Database;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Driver;

public class RuntimeDriverLoader {
	public Driver loadDriver(String path) {
		URL u = null;
		Driver d = null;
		try {
			u = new URL("jar:file:" + path + "!/");
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String classname = "com.mysql.jdbc.Driver";
		URLClassLoader ucl = new URLClassLoader(new URL[] { u });

		try {
			d = (Driver)Class.forName(classname, true, ucl).newInstance();
		} catch (InstantiationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return d;
	}
}
