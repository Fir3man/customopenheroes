package Database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Player.Character;
import Player.Player;

public class CharacterDAO {
	
	/*
	 * Method to add new character entry in database. Also return character instance.
	 * Reason for instantiating Character here: database will implicitly ensure unique character ID.
	 * Up until adding new database entry for the character, we have no way of knowing it's ID-to-be.
	 * Add entry -> ID assigned -> retrieve entry with unique ID -> ??? -> profit
	 * Return: Character instance if success, null if failed
	 */
	public static Character addAndReturnNewCharacter(String name, byte[] stats, int chClass, byte statpoints, Player pl, int xCoords, int yCoords) {
		try {
			if(Queries.newCharacter(new SQLconnection().getConnection(), name, stats, chClass, statpoints, xCoords, yCoords, pl.getAccountID()).execute()) {
				ResultSet rs = Queries.getCharacterByName(new SQLconnection().getConnection(), name).executeQuery();
				boolean gotResults = rs.next();
				if(gotResults) {
					Character newCharacter = processCharacterTable(rs);
					newCharacter.setPlayer(pl);
					return newCharacter;
				} else {
					return null;
				}
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/*
	 * Load a single character with given name
	 * Return: Character object if found, null if no entry
	 */
	public static Character loadCharacter(String name) {
		ResultSet rs;
		try {
			rs = Queries.getCharacterByName(new SQLconnection().getConnection(), name).executeQuery();
		boolean gotResults = rs.next();
		if(gotResults) {
			Character newCharacter = processCharacterTable(rs);
			return newCharacter;
		} else {
			return null;
		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	/*
	 * Load a single character with given id
	 * Return: Character object if found, null if no entry
	 */
	public static Character loadCharacter(int id) {
		ResultSet rs;
		try {
			rs = Queries.getCharacterByID(new SQLconnection().getConnection(), id).executeQuery();
		boolean gotResults = rs.next();
		if(gotResults) {
			Character newCharacter = processCharacterTable(rs);
			return newCharacter;
		} else {
			return null;
		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	/*
	 * Load list of characters with given account ID
	 * Return: List containing Character objects, or null if failed
	 */
	public static ArrayList<Character> loadCharacters(int id) {
		ArrayList<Character> chlist = new ArrayList<Character>();
		ResultSet rs;
		try {
			rs = Queries.getCharactersByOwnerID(new SQLconnection().getConnection(), id).executeQuery();
				while(rs.next()) {
					chlist.add(processCharacterTable(rs));
					System.out.println("Returned relation size: " + rs.getMetaData().getColumnCount());
				}
			if(!chlist.isEmpty()) {
				return chlist;
			} else {
				return null;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	/*
	 * Save given character object to database
	 * Return: true if success, false if fail
	 */
	public static boolean saveCharacter(Character chara) {
		return true;
	}
	
	/*
	 * Save a list of characters to database
	 * Return: true if all succeed, false if one or more fail
	 */
	public static boolean saveCharacters(List<Character> ch) {
		return true;
	}
	
	public static Character processCharacterTable(ResultSet rs) {
		Character newCharacter = new Character();
		
		try {
			newCharacter.setuid(rs.getInt(1));
			newCharacter.setName(rs.getString(2));
			newCharacter.setCharacterClass(rs.getInt(3));
			newCharacter.setFaction(rs.getInt(4));
			newCharacter.setLevel(rs.getInt(5));

			/*
			 * TODO: add dem hp etc stuffs
			 */
			newCharacter.setX(rs.getInt(17));
			newCharacter.setY(rs.getInt(18));
		
			int[] dats = new int[5];
		
			dats[0] = rs.getInt(19); //INT 
			dats[2] = rs.getInt(20); //VIT 
			dats[1] = rs.getInt(21); //AGI 
			dats[4] = rs.getInt(22); //STR 
			dats[3] = rs.getInt(23); //DEX 
		
			newCharacter.setStats(dats);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newCharacter;
	}
}
