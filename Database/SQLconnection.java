package Database;


import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

import Configuration.Configuration;
import Configuration.ConfigurationManager;


public class SQLconnection {
	  private Configuration conf;
	  
	  SQLconnection(){
		  this.conf = ConfigurationManager.getConf("GameServer");
	  }

	  public Connection getConnection() {
	    String url = "jdbc:mysql://" + this.conf.getVar("host")  + "/" + this.conf.getVar("db");
	    
	    Driver driver = new RuntimeDriverLoader().loadDriver(this.conf.getVar("driver"));
	    Connection conn = null;
	    
		try {
			DriverManager.registerDriver(new DriverWrapper(driver));
			conn = DriverManager.getConnection(url, this.conf.getVar("username"), this.conf.getVar("password"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return conn;
	  }
}
