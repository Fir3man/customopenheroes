package Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Queries {
	public static PreparedStatement auth(Connection sqlc, String username, String hash) throws Exception {
		PreparedStatement st = sqlc.prepareStatement("SELECT * FROM accounts WHERE username = ? AND password = ?;");
		st.setString(1, username);
		st.setString(2, hash);		
		return st;
	}
	
	public static PreparedStatement newCharacter(Connection sqlc, String name, byte[] stats, int chClass, byte statpoints, int xCoords, int yCoords, int owner) {
		PreparedStatement st = null;
		try {
			st = sqlc.prepareStatement("INSERT INTO " +
					"characters(charname, charClass, faction, level, maxHP, currentHP, maxMana, currentMana, maxStamina, currentStamina, attack, defence, fame, flags, locationX, locationY, map, intelligence, vitality, agility, strength, dexterity, statpoints, skillpoints, ownerID) " +
					"VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
	
		st.setString(1, name);  //character name
		st.setInt(2, chClass);  //class
		st.setInt(3, 0);	    //faction
		st.setInt(4, 1);	    //level
		st.setInt(5, 10);	    //max HP
		st.setInt(6, 10);		//current HP	 	
		st.setInt(7, 10);		//max mana
		st.setInt(8, 10);		//current mana
		st.setInt(9, 10);		//max stamina
		st.setInt(10, 10);		//current stamina
		st.setInt(11, 10);		//attack
		st.setInt(12, 10);		//defence
		st.setInt(13, 0);		//fame
		st.setInt(14, 10);		//flags
		
	
		
		st.setInt(15, xCoords);		//x coords
		st.setInt(16, yCoords);		//y coords
		
		st.setInt(17, 10);		//map
		
		st.setInt(18, (int)stats[0]);		//INT
		st.setInt(20, (int)stats[1]);		//AGI
		st.setInt(19, (int)stats[2]);		//VIT
		st.setInt(22, (int)stats[3]);		//DEX
		st.setInt(21, (int)stats[4]);		//STR
		
		st.setInt(23, (int)statpoints);		//statpoints
		st.setInt(24, 0);					//skillpoints
		st.setInt(25, owner); //owner account
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return st;
	}
	
	public static PreparedStatement getCharacterByName(Connection sqlc, String name) throws Exception {
		PreparedStatement st = sqlc.prepareStatement("SELECT * FROM characters WHERE charname = ?;");
		st.setString(1, name);
		return st;
	}
	
	public static PreparedStatement getCharacterByID(Connection sqlc, int id) throws Exception {
		PreparedStatement st = sqlc.prepareStatement("SELECT * FROM characters WHERE CharacterID = ?;");
		st.setInt(1, id);
		return st;
	}
	
	public static PreparedStatement getCharactersByOwnerID(Connection sqlc, int id) throws Exception {
		PreparedStatement st = sqlc.prepareStatement("SELECT * FROM characters WHERE ownerID = ?;");
		st.setInt(1, id);
		return st;
	}
}
