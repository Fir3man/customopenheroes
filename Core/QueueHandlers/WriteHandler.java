package Core.QueueHandlers;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.concurrent.LinkedBlockingQueue;

import Connections.Connections;
import Core.Server.Core.ServerCore;




public class WriteHandler implements Runnable {
	private Connections con;
	private SocketChannel cn;
	
	
	public WriteHandler(SocketChannel sc, Connections cn) {
		this.cn = sc;
		this.con = cn;
	}
	
	public void run() {
			SocketChannel current = this.cn;
			if(this.con.isChannelRegistered(current)) {
			LinkedBlockingQueue<ByteBuffer> writeBuffer = this.con.getConnection(current).getWriteBuffer();
			if(!writeBuffer.equals(null)) {
				while(this.con.getKeyByChannel(current).isWritable() && !writeBuffer.isEmpty()) {
						ByteBuffer tmp = writeBuffer.poll();
						//Block while writing. Explicitly make sure entire buffer is written.
						while(tmp.remaining() > 0) { 
							try {
								current.write(tmp);
							} catch (IOException e) {
								ServerCore.finalizeConnection(this.cn);
							} catch (NullPointerException np) {
								ServerCore.finalizeConnection(this.cn);
							}
						}
				}
			}
			}
				//make sure to flip interestops back to read after writing
				current.keyFor(Connections.getSelector()).interestOps(SelectionKey.OP_READ);
				
	}
}
