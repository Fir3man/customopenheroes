package Core.QueueHandlers;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.LinkedBlockingQueue;

import Connections.Connections;
import Core.PacketHandler.PacketHandler;
import Core.Server.Core.ServerCore;





public class ReadHandler implements Runnable {
	private Connections con;
	private SocketChannel cn;
	private PacketHandler han;
	
	
	public ReadHandler(SocketChannel sc, Connections cn, PacketHandler ph) {
		this.cn = sc;
		this.con = cn;
		this.han = ph;
	}
	
	public void run() {
			SocketChannel current = this.cn;
		if(this.con.isChannelRegistered(current)) {
				LinkedBlockingQueue<ByteBuffer> readBuffer = this.con.getConnection(current).getReadBuffer();
				if(!readBuffer.equals(null)) {
					int numRead=0;			
					ByteBuffer buff = ByteBuffer.allocate(1024);
				
					try {
						numRead = current.read(buff);	
						if (numRead == -1) { //if client has disconnected, finalize the key/channel/thread
							try {
								ServerCore.finalizeList.put(current);
							} catch (InterruptedException e) {
								// 	TODO Auto-generated catch block
								e.printStackTrace();
							}				
						} else {
							buff.flip();
							try {
								if(buff.remaining() > 0) {
									readBuffer.put(buff);
									this.han.processList(this.con.getConnection(current));
								}
							} catch (InterruptedException e) {
								// 	TODO Auto-generated catch block
								e.printStackTrace();
							} 
						}
					} catch (IOException e) {
						e.printStackTrace();
						ServerCore.finalizeConnection(this.cn);
					} catch (NullPointerException np) {
						ServerCore.finalizeConnection(this.cn);
					}										
		
				}
		}
		
	}
}
