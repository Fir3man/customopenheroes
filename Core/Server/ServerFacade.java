package Core.Server;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.LinkedBlockingQueue;

import Connections.Connection;
import Connections.Connections;
import Core.PacketHandler.PacketHandler;
import Core.Server.Core.ServerCore;
import Core.ThreadPools.PacketHandlerPool;



/**
 * This class acts as facade between essential server core modules.
 * Use this as an interface for your convenience!
 */

public class ServerFacade {
	private Connections con;
	private PacketHandlerPool ppool; 
	private ServerCore sc;
	private PacketHandler pckth;
	
	/*
	 * Will start a listening socket with given thread pool size and initial concurrent connections capacity
	 * int threadCount = thread pool size
	 * int initCap = initial connection capacity 
	 */
	public ServerFacade(InetSocketAddress isa, int threadCount, int initCap, PacketHandler p) {
		this.con = new Connections(initCap, 50, threadCount);
		this.ppool = new PacketHandlerPool(threadCount);
		this.pckth = p;
		this.pckth.initialize(this);
		this.sc = new ServerCore(isa, this.ppool, this.con, this.pckth);
	}
	
	public int getConnectionCount() {
		return this.con.getConnectionCount();
	}
	
	public void stopServer() {
		ServerCore.setRunning(false);
	}
		
	public PacketHandlerPool getPpool() {
		return ppool;
	}
		
	public void finalizeConnection(SocketChannel chan) {
		ServerCore.finalizeConnection(chan);
	}
	
	public Selector getSelector() {
		return this.sc.getSelector();
	}
	
	public ServerSocket getServerSocket() {
		return this.sc.getsSocket();
	}
	
	public LinkedBlockingQueue<ByteBuffer> getConnectionReadQueue(SocketChannel chan) {
		return this.con.getConnection(chan).getReadBuffer();
	}
	
	public LinkedBlockingQueue<ByteBuffer> getConnectionWriteQueue(SocketChannel chan) {
		return this.con.getConnection(chan).getWriteBuffer();
	}
	
	public int getReadQueueSize() {
		return this.sc.getReadQsize();
	}
	
	public int getWriteQueueSize() {
		return this.sc.getWriteQsize();
	}
	
	public void setReadQueueSize(int s) {
		this.sc.setReadQsize(s);
	}
	
	public void setWriteQueueSize(int s) {
		this.sc.setWriteQsize(s);
	}

	public PacketHandler getPckth() {
		return pckth;
	}

	public void setPckth(PacketHandler pckth) {
		this.pckth = pckth;
	}
	
	public Connection getConnectionByChannel(SocketChannel sc) {
		return this.con.getConnection(sc);
	}
	
	public ConcurrentMap<SocketChannel, Connection> getConnections() {
		return this.con.getConnectionMap();
	}
	
}

