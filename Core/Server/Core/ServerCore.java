package Core.Server.Core;


import java.io.IOException;
import java.nio.channels.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.Set;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;

import Connections.Connections;
import Core.ThreadPools.PacketHandlerPool;
import Core.PacketHandler.PacketHandler;
import Core.QueueHandlers.ReadHandler;
import Core.QueueHandlers.WriteHandler;



public class ServerCore {
	private Selector selector;
	private ServerSocket sSocket;
	private PacketHandlerPool php; 
	public static boolean running = true;
	
	private int readQsize = 100; //default read/write queue size for each
	private int writeQsize = 100; //new connection. can be adjusted while running(applies to new connections)
	
	private Connections con; 
	private PacketHandler phand;
	
	public static volatile LinkedBlockingQueue<SocketChannel> finalizeList = new LinkedBlockingQueue<SocketChannel>();
	
	
	public ServerCore(InetSocketAddress bind, PacketHandlerPool ppool, Connections con, PacketHandler ph) {	
		this.php = ppool;
		this.phand = ph;
		this.init(bind, con);
	}
	
	//method for initializing listening socket
	private void init(InetSocketAddress bind, Connections con) {		
		try {
			ServerSocketChannel ssChannel = ServerSocketChannel.open(); 
			ssChannel.configureBlocking(false); //set as non-blocking
			this.selector = Selector.open();
			ssChannel.register(this.selector, SelectionKey.OP_ACCEPT); //set ops to accept connections		
			this.sSocket = ssChannel.socket(); //get socket	
			this.sSocket.bind(bind); //bind to ip/port
			Connections.setSelector(this.selector);
			this.con = con;
			
			System.out.println("New listening socket at port: " + this.sSocket.getLocalPort());
			
		} catch (ClosedChannelException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.makeItSo();
	}
	
	//captain Jean Luc Picard
	private void makeItSo() {
		while(running) {
			try {
				this.selector.select(300); //block for 300ms at most and select all channels ready for operations
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Set<SelectionKey> readySet = this.selector.selectedKeys();
			Iterator<SelectionKey> keySetIter = readySet.iterator();
			
			synchronized(finalizeList) {
				while(!finalizeList.isEmpty()) {
					SocketChannel tmp = finalizeList.poll();
					this.con.removeConnection(tmp);
					try {
						System.out.println("Closing connection: " + tmp);
						if(tmp.isConnected()) {
							tmp.close();
						}
						tmp.keyFor(this.selector).cancel();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
			//Iterate all selected keys
			while(keySetIter.hasNext()) {
				SelectionKey key = keySetIter.next();
				
				//always a good idea to check validity of dem keys when dealin' with em
				//a key is valid if it's channel is open and the key's not canceled
				if(key.isValid()) {
					
					//channel is ready to accept new connection
					if(key.isAcceptable()) {
						try {
							ServerSocketChannel server = (ServerSocketChannel)key.channel();
							SocketChannel client = server.accept();	
							client.configureBlocking(false); //set as non-blocking	       
							client.register(this.selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE); //set valid ops for the channel as read/write
							
							this.phand.newConnection(this.con.addAndReturnConnection(client, this.readQsize, this.writeQsize));
							System.out.println("New connection: " + client);
						} catch (ClosedChannelException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else 
					//ready for reading	
					if(key.isReadable()) {
						SocketChannel tmp = (SocketChannel)key.channel();
						if(this.con.isChannelRegistered(tmp)) {
							this.php.executeProcess(new ReadHandler(tmp, this.con, this.phand));
						}
					} else
					//ready for writing
					if(key.isWritable()) {
						SocketChannel tmp = (SocketChannel)key.channel();
						if(this.con.isChannelRegistered(tmp)) {
							this.php.executeProcess(new WriteHandler(tmp, this.con));
						}	
					}
						
				}
				
				keySetIter.remove();
				
			}
		}
	}
	
	//set as false to exit main loop
	public static void setRunning(boolean val) {
		running = val;
	}

	public Selector getSelector() {
		return selector;
	}

	public ServerSocket getsSocket() {
		return sSocket;
	}

	public static boolean isRunning() {
		return running;
	}


	public static LinkedBlockingQueue<SocketChannel> getFinalizeList() {
		return finalizeList;
	}
	
	public static synchronized void finalizeConnection(SocketChannel chan) {
		try {
			if(!finalizeList.contains(chan) && chan.isConnected()) {
				finalizeList.put(chan);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getReadQsize() {
		return readQsize;
	}

	public void setReadQsize(int readQsize) {
		this.readQsize = readQsize;
	}

	public int getWriteQsize() {
		return writeQsize;
	}

	public void setWriteQsize(int writeQsize) {
		this.writeQsize = writeQsize;
	}

	public PacketHandler getPacketHandler() {
		return phand;
	}

	public void setPacketHandler(PacketHandler phand) {
		this.phand = phand;
	}


}

