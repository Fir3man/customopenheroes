package World;

import java.util.*;

// Grid.class
// Each grid represents greater area, usually a in-game map
// Grid consists of multiple Area objects. Grid is always presument to be square and therefore it should have gridsize * gridsize of Area objects stored.
// Grid is unaware of what objects are within each Area, it merely serves as means for the Objects to retrieve proper Area object

public class Grid
{
    private Map<Object, Area> grid = Collections.synchronizedMap(new HashMap<Object, Area>());	
	private int uid;
	public int areaSize = 200;
	public int gridSize = 500;
	private float[] dimenssions;
	private String name;
	private float mx, my;
	

	// Initialiazes new grid
	// id  -  unique id of this grid
	// gsize - size of one side of the grid
	// areasize - size of one side of each Area
	// name  - name of the grid
	// x  - x-coordinate in-game from which the grid begins
	// y  - y-coordinate in-game from which the grid begins
	// poolSize - number of threads allocated for MobControllers in this grid
	public Grid(int id, int gsize, int areasize, String name, float x, float y, int poolSize)
    {
	  System.out.print("Creating Grid for map " + name + " id " + id + " ... ");
      this.uid = id;
	  this.setName(name);
	  this.gridSize = gsize;
	  this.mx = x;
	  this.my = y;
	  this.areaSize = areasize;
	  this.initGrid();
    }
	// add new area to the grid
    private void addArea(Area a)
	{
	 int tmp[] = a.getcoords();
	 int foo = ((tmp[0] *this.gridSize) + tmp[1] );
	 // System.out.println("Addin coords ("+tmp[0]+","+tmp[1]+") uid:" + foo);
	 if(this.areaExists(tmp)){ System.out.println("FUUUUUUUUuuuu no uniqs"); }
	 grid.put(foo, a);
	}
	// returns true if area in coordinates coords[] does exists, otherwise returns false
	public boolean areaExists(int []coords)
	{
	  int foo = (coords[0] * this.gridSize) + coords[1];
	  return grid.containsKey(foo);
	}
	// returns area that is in coordinates coords[]
	public Area getArea(int [] coords)
	{
	  int foo = (coords[0] * this.gridSize) + coords[1];
	  return grid.get(foo);
	}
	public void loadGrid()
	{
	  
	}
	// returns the unique ID of this grid
	public int getuid()
	{
	  return this.uid;
	}
	// initializes all the necessary Area objects needed for functionality
	private void initGrid()
	{
	  Area tmp;
	  int count = 0;
	  float x = this.mx;
	  float y = this.my;
	  for (int i =0; i < this.gridSize; i++)
	  {
	    for (int u =0; u < this.gridSize; u++)
		{
		  // System.out.println("Creating area ("+u+","+i);
	      tmp = new Area(u, i, this.gridSize, this.areaSize, x, y);
		  tmp.setuid((u * this.gridSize) + i);
		  this.addArea(tmp);
		  tmp = null;
		  count++;
		  x += this.areaSize;
		}
		x = this.mx;
		y += this.areaSize;
	  }
	  System.out.println("Done " + count + " Areas successfully created");
	  
	}
	public float[] getDimenssions() {
		return dimenssions;
	}
	public void setDimenssions(float[] dimenssions) {
		this.dimenssions = dimenssions;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	/*
	public void update(int id, int [] coords)
	{
	  Map<Integer, Object> near = Collections.synchronizedMap(new HashMap<Integer, Object>());	
	  // Map<Integer, Object> foo = Collections.synchronizedMap(new HashMap<Integer, Object>());	
	  Area tmp;
	  
	  // set bounries for the send
	  int sx = coords[0] -1;
	  int sy = coords[1] -1;
	  int ex = coords[0] +1;
	  int ey = coords[1] +1;
	  
	  // check that we stay within the grid
	  if (sx < 0 ) sx +=1;
	  if (sy < 0 ) sy +=1;
	  if (ex > this.gridSize) ex -= 1;
	  if (ey > this.gridSize) ey -= 1;
	  
	  // retrieve all objects with in range
	  for (int i=sy; i <= ey; i++)
	  {
	    for (int u=sx; u <=ex; u++)
		{
		  int []t = new int []{u, i};
		  tmp = this.getArea(t);
		  near.putAll(tmp.getMemberMap());
		}
	  }
	  
	  
	  // foo.putAll(near);
	  location obj;
	  location ob;
	  boolean foo = false;
	  int key;
	  int ikey;
	
	synchronized(near) {  //synchronized iteration for thread safe operations
	  Iterator it = near.entrySet().iterator();
	  Iterator ut = near.entrySet().iterator();
	  while (it.hasNext()) {
	    Map.Entry pairs = (Map.Entry)it.next();
	    key = (Integer)pairs.getKey();
	    obj  = (location)pairs.getValue();
		foo = true;
	    if(obj.GetChannel() != null)
		{
		  while (ut.hasNext()) {
	       Map.Entry pairsi = (Map.Entry)ut.next();
	       ikey = (Integer)pairsi.getKey();
	       ob  = (location)pairsi.getValue();
		   if (obj.getuid() != id)
		   {
		     // send player Obj
		     ob.move(obj.GetChannel());
		   }
		 }
		}
	  }
	}
	  
	  
	}
	*/
}
