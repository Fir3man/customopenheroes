package World;

import java.util.*;


import Core.ThreadPools.MobThreadPool;
import Mob.Mob;
import Mob.MobController;
import Player.Character;

// WMap.class
// Static resource that keeps track of all the grids, connected Characters and mobs inside the game


public class WMap
{
	public static final Map<Integer, Grid> grids = Collections.synchronizedMap(new HashMap<Integer, Grid>());	
    public static final Map<Integer, Character>  Characters = Collections.synchronizedMap(new HashMap<Integer, Character>());	
	public static final float viewDistance = 150;
	public static int nextFreeUid = 1;
	public static int gridSize = 100;
	public static int gridsPerSide = 500;
	public static Map<Integer, Mob> mobs = Collections.synchronizedMap(new HashMap<Integer, Mob>());
	private static MobThreadPool mobPool = new MobThreadPool(50);
	
	// add mob that has been created to the list
	
	public static void AddMob(int uid, Mob mb) {
		mobs.put(uid, mb);
	}
	// returns Mob identified by uid
	public static MobController GetMobController(int uid) {
		return mobs.get(uid).getControl();
	}
	public static boolean mobExists(int uid){
		return mobs.containsKey(uid);
	}
	// add grid that has been created to the list
	public static void addGrid(Grid g)
	{
	 int tmp = g.getuid();
	 System.out.println("Added grid " + tmp +" To wmap");
	 grids.put(tmp, g);
	}
	// returns true if Grid identified by uid exists in the WMap, false if not
	public static boolean gridExist(int uid)
	{
	  return grids.containsKey(uid);
	}
	// adds new Character to the list
	public static void addCharacter(Character obj)
	{
	 //int tmp = obj.getuid();
	 //System.out.println("New Character "+ tmp+" added to list");
	 //Characters.put(tmp, obj);
	}
	// returns Character identified by uid
	public static Character getCharacter(int uid)
	{
	  return (Character)Characters.get(uid);
	}
	// return true if Character obj is in the list, otherwise returns false
	public static boolean CharacterExists(Character obj)
    {
      return Characters.containsValue(obj);
    }
	// returns true if Character with UID id exists in the list, otherwise returns false
    public static boolean CharacterExists(int id)
    {
      return Characters.containsKey(id);
    } 
	// removes Character identified by uid from the list
	public static void rmCharacter(int uid)
	{
	  Characters.remove(uid);
	}
	// returns Map containing all the Characters currently in the game
	public static Map<Integer, Character> getCharacterMap()
	{ 
	  return Characters;
	}
	/**
	 *  OBSOLETE: generates new uid
	 *
	public static int getNewUid()
	{
	 int tmp;
	 do
	 {
	 tmp = nextFreeUid;
	 nextFreeUid++;
	 } while (Characters.containsKey(nextFreeUid) || Mobs.containsKey(nextFreeUid));
	 return tmp;
	}*/
	// returns the Grid designated by the uid
	public static Grid getGrid(int uid)
	{
	  return grids.get(uid);
	}
	// return map containing all the grids
	public static Map<Integer, Grid> returnMap()
	{
	  return grids;
	}
	public static MobThreadPool getMobPool() {
		return mobPool;
	}
	// calculates in-game distance between point a and b
	public static float distance(float a, float b)
	{
	   float result;
	   if (a >= 0 && b >= 0) result = a - b;
	   else if (a >= 0 && b < 0 ) result = a + Math.abs(b);
	   else if (a < 0 && b >= 0) result = b + Math.abs(a);
	   else result = Math.abs(a) - Math.abs(b);
	   
	   return Math.abs(result);
	
	}
	// calculates in-game distance between coordinates (tx,ty) and (dx,dy)
	public static float distance(float tx, float ty, float dx, float dy)
	{
	  return ((float) (Math.sqrt( (Math.pow( (double)(tx - dx), (double)2 ) + Math.pow( (double)(ty - dy), (double)2) )) ));	  
	}
	
	
}
 




















