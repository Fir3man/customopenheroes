package World;

import java.util.*;
import java.nio.ByteBuffer;
import java.nio.channels.*;

import Mob.MobController;


// Area.class
// Keeps track of players withhin small area in side the game
// Each Area are members of some Grid, but Area is unaware of which
// Area is always a square as is its Grid

public class Area
{
  private Map<Integer, Location> members = Collections.synchronizedMap(new HashMap<Integer, Location>());
  // private Map<Integer, Object> near = Collections.synchronizedMap(new HashMap<Integer, Object>());	
  private List<SocketChannel> playerList = Collections.synchronizedList(new ArrayList<SocketChannel>());
  
  private int size;
  private int gridSize;
  private float x,y;
  private int gridx,gridy;
  private int uid;
  // private Grid myGrid;
  
  // initializes Area
  // parameter descriptions:
  // gx, gy  -  (x,y) coordinates of the area withing the grind that it belongs to
  // grindsize  - size of grids one side
  // asize   -  size of the areas one side
  // sx, sy  - in-game coordinates associated to this Area
  public Area(int gx, int gy, int gridsize, int asize, float sx, float sy)
  {
	this.size = asize;
	this.gridSize = gridsize;
	// System.out.print(".");
	this.x = sx;
	this.y = sy;
	this.gridx = gx;
	this.gridy = gy;
	// this.myGrid = WMap.getGrid(gridId);
  }
  // adds new member (player, mob, npc, droped item) to the Area
  public void addMember(Location ob)
  {
    // System.out.println("Item added to area " + this.uid);
    this.members.put(ob.getuid(), ob);
	if (ob.GetChannel() != null) { 
	 this.playerList.add(ob.GetChannel());
	}
  }
  public void setuid(int id)
  {
    this.uid = id;
  }
  public int getuid()
  {
    return this.uid;
  }
  // removes existing member from Area
  public void rmMember(int uid)
  {
    // System.out.println("Item removed from area" + this.uid);
	if(members.containsKey(uid)) {
		if (this.getMember(uid).GetChannel() != null) { 
			if(this.playerList.contains(this.getMember(uid).GetChannel())) {
				this.playerList.remove(this.playerList.indexOf(this.getMember(uid).GetChannel())); 
			}
		}
		members.remove(uid);
	}
	
  }
  // returns list containing all the players in this Area
  public List<SocketChannel> returnPlayerList() {
	return this.playerList;
  }
  // returns true if there are players in area, false if not
  public boolean hasPlayers()
  {
    return !playerList.isEmpty();
  }
  // returns true if Area has any members at all, false if not
  public boolean hasMembers()
  {
    return !this.members.isEmpty();
  }
  public Location getMember(int uid)
  {
    return (Location)members.get(uid);
  }
  // returns true if Area has any members at all, false if not
  public Map<Integer, Location> getMemberMap()
  {
    return members;
  }
  public int[] getcoords()
  {
    int [] tmp = new int []{this.gridx, this.gridy};
    return tmp;
  }
  // return true if obj is member of this Area, else returns false
  public boolean isMember(Location obj)
  {
    return members.containsValue(obj);
  }
  // returns true if member whose uid is id is member of this Area, else returns false
  public boolean isMember(int id)
  {
    return members.containsKey(id);
  }
  // send packet buf to all members except one specified by uid
  public void sendToMembers(int uid, ByteBuffer buf)
  {
	  SocketChannel tmp;
	  MobController cont;
	  Location loc;
	  
	  synchronized(this.members)
	  {
		  Iterator<Map.Entry<Integer, Location>> iter = this.members.entrySet().iterator();
		  while(iter.hasNext()) {
			  Map.Entry<Integer, Location> pairs = iter.next();
			  loc = pairs.getValue();
			  tmp = loc.GetChannel();
			  
			  if (tmp == null){
				  if (WMap.mobExists(pairs.getKey())){
					  cont = WMap.GetMobController(pairs.getKey());
					  if (!cont.isActive()) WMap.getMobPool().executeProcess(cont);
				  }
			  }
			  else if (loc.getuid() != uid){
				  // write buf to player loc
			  }
		  }
		  
	  }
  }
  // calculates if member whose uid is id should still be member of this Area
  // returns the coordinates of Area the id should be member of
  public int[] update(int id)
  {
    if (!this.isMember(id)){ return (new int[]{-1,-1}); }
    Location tmp = (Location)members.get(id);
    float tx = tmp.getlastknownX();
	float ty = tmp.getlastknownY();
	int []ret = new int[]{this.gridx, this.gridy};
	// int id = this.uid;
	// System.out.println("Area: "+ this.uid+ " NOTICE UID: " + id +" called update() from X: " +tx+" Y:" + ty);
	// System.out.println("Area:" + this.uid +" Grid position ("+ ret[0]+"," + ret[1] +") update() from " + id);
	// System.out.println("Player " +id +" X:" +tx + " Y: " +ty);
	// System.out.println("Area:" +this.uid +" start X:" +this.x+" Start Y:" +this.y+" End X" + (this.x+this.size) + " End Y:" + (this.y +size));
	if (tx < this.x)
	{
	  // System.out.println("Player X < area start X");
	  if (ty < this.y)
	  {  
	    // System.out.println("Player Y < area start Y");
	    if((this.gridx -1) >= 0 && (this.gridy -1) >= 0)
		{ 
		  // System.out.println("Grid X -1 & Grid Y -1");
		  ret[0] -= 1; 
		  ret[1] -= 1; 
		} 
	  }
	  else if ( ty >= this.y && ty <= (this.y + this.size))
	  { 
	    if((this.gridx -1 ) >= 0){ 
		  ret[0] -=1;  
		}
	  } 
	  else if (ty > (this.y + this.size)){ if ( (this.gridy +1) <= this.gridSize){ ret[0] -=1; ret[1] +=1; } }
	}
	else if (tx > (this.x +this.size))
	{
	  // System.out.println("Player X > area end X");
	  if(ty < this.y ){ if((this.gridx +1) <= this.gridSize && (this.gridy -1) >= 0){ ret[0] += 1; ret[1] -=1; } }
	  else if (ty >= this.y && ty <= (this.y + this.size)){ if((this.gridx +1) <= this.gridSize){ ret[0] += 1; } }
	  else if ( ty > (this.y + this.size)){ if((this.gridx +1) <= this.gridSize && (this.gridy +1) <= this.gridSize){ ret[0] +=1; ret[1] +=1; } }
	}
	else if (tx >= this.x && tx <= (this.x + this.size))
	{
	  // System.out.println("area startx =< Player X <= area end X");
	  if (ty < this.y){ if ((this.gridy - 1) >= 0){ ret[1] -=1; } }
	  if (ty > (this.y + this.size)) { if ((this.gridy +1) <= this.gridSize) { ret[1] += 1; } }
	}
	// System.out.println("Area:" + this.uid +" Player" +id + " move to Grid position ("+ ret[0]+"," + ret[1] +") update() completed");
	return ret;
  }

}
