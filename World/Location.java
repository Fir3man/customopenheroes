package World;

import java.nio.ByteBuffer;
import java.nio.channels.*;

// <<Interface>>location
// Interface for accessing all the objects within the in-game map stored in the grid system


public interface Location
{
  public int getuid();
  public void setuid(int uid);
  public float getlastknownX();
  public float getlastknownY();
  public SocketChannel GetChannel();
  public short getState();
  public ByteBuffer getInitPacket();
}