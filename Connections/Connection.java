package Connections;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.concurrent.LinkedBlockingQueue;

import Core.Queue.PacketQueue;



public class Connection {
	private SocketChannel chan;
	private String ip;
	private PacketQueue pcktq;
	
	
	public Connection(SocketChannel sc, int rsize, int wsize) {
		this.chan = sc;
		this.ip = sc.socket().getInetAddress().getHostAddress();
		this.pcktq = new PacketQueue(rsize, wsize);
	}

	public SocketChannel getChan() {
		return chan;
	}

	public String getIp() {
		return ip;
	}
	
	public LinkedBlockingQueue<ByteBuffer> getReadBuffer() {
		return this.pcktq.getReadBuffer();
	}
	
	public LinkedBlockingQueue<ByteBuffer> getWriteBuffer() {
		return this.pcktq.getWriteBuffer();
	}
	
	public void addRead(ByteBuffer bboss) {
		this.pcktq.getReadBuffer().offer(bboss);
	}

	public void addWrite(ByteBuffer bboss) {
		this.pcktq.getWriteBuffer().offer(bboss);
		this.chan.keyFor(Connections.getSelector()).interestOps(SelectionKey.OP_WRITE);
	}
}
