package GameServer;
import java.net.InetSocketAddress;

import Configuration.Configuration;
import Configuration.ConfigurationManager;
import Core.Server.ServerFacade;

public class StartGameserver {
		public static void main(String[] args) {
			ConfigurationManager.init("server.xml", true);
			Configuration conf = ConfigurationManager.getConf("GameServer");
			new ServerFacade(new InetSocketAddress(conf.getIntVar("port")), 100, 300, new Gameserver()); 
		}
}
