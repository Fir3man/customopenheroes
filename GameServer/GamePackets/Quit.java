package GameServer.GamePackets;

import java.nio.ByteBuffer;

import Connections.Connection;
import Core.Server.Core.ServerCore;

public class Quit implements Packet {

	@Override
	public void execute(ByteBuffer buff) {
		// TODO Auto-generated method stub
		
	}


	public ByteBuffer returnWritableByteBuffer(ByteBuffer buffyTheVampireSlayer, Connection con) {
		final byte[] quit = new byte[] {(byte)0x09, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x03, (byte)0x00, (byte)0x64, (byte)0x00, (byte)0x00};
		ServerCore.finalizeConnection(con.getChan());
		return ByteBuffer.wrap(quit);
	}
	
}
