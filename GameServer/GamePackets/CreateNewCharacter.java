package GameServer.GamePackets;

import java.nio.ByteBuffer;

import Connections.Connection;
import Database.CharacterDAO;
import Encryption.Decryptor;
import Player.PlayerConnection;
import Player.Character;
import Tools.BitTools;

public class CreateNewCharacter implements Packet {


	public void execute(ByteBuffer buff) {

	}

	public ByteBuffer returnWritableByteBuffer(ByteBuffer buffyTheVampireSlayer, Connection con) {
		final byte[] createNewCharacter = new byte[] { //static packet to respond to clients' character creation request
				(byte)0x14, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x03, (byte)0x00, (byte)0x06, (byte)0x00, (byte)0x01, (byte)0x01, 
				(byte)0x12, (byte)0x2b, (byte)0x00, (byte)0xc0, (byte)0xb7, (byte)0xc4, (byte)0x00, (byte)0xe0, (byte)0x21, (byte)0x45 
		};
		
		byte[] decrypted = new byte[(buffyTheVampireSlayer.get(0) & 0xFF)-8];

		for(int i=0;i<decrypted.length;i++) {
			decrypted[i] = (byte)(buffyTheVampireSlayer.get(i+8) & 0xFF);
		}
		decrypted = Decryptor.Decrypt(decrypted);
		
		byte[] characterName = new byte[16]; //if memory serves - max name length = 13 characters
		byte characterClass;
		byte[] stats = new byte[] { (byte)0x0A, (byte)0x0A, (byte)0x0A, (byte)0x0A, (byte)0x0A };
		byte statusPointsleft = 0x05;
		byte[] realLengthname = null;
		
		for(int i=0;i<characterName.length;i++) {
			characterName[i] = decrypted[i];
			if(decrypted[i] == 0x00) {
				realLengthname = new byte[i];
				break; 
			}
		}
		
		for(int i=0;i<realLengthname.length;i++) {
			realLengthname[i] = characterName[i];
		}
		
		/*
		 * Character bytes:
		 * 01 = Warrior
		 * 02 = Assassin
		 * 03 = Mage
		 * 04 = Monk
		 */
		characterClass = decrypted[decrypted.length-14]; 
		
		if(decrypted[36] <= 0x05) { //remember to make sure that no more than 5 points can be left after creating character
			statusPointsleft = decrypted[decrypted.length-2]; 
		} else {
			//The player is cheating. Do something about it here
		}
		
		stats[0] = decrypted[decrypted.length-4]; //INT
		stats[1] = decrypted[decrypted.length-6]; //AGI
		stats[2] = decrypted[decrypted.length-8]; //VIT
		stats[3] = decrypted[decrypted.length-10];//DEX
		stats[4] = decrypted[decrypted.length-12];//STR
		
		
		/*
		 * TODO: Find out which is which stat
		 */
		
		String name = new String(realLengthname);
		
		byte[] Xcoordinates = new byte[] {
				(byte)0x00, (byte)0x80, (byte)0xcf, (byte)0xc4   
		};
		byte[] Ycoordinates = new byte[] {
				(byte)0x00, (byte)0x80, (byte)0x12, (byte)0x45
		};
		
		Character returnedCharacter = CharacterDAO.addAndReturnNewCharacter(name, stats, characterClass, statusPointsleft, ((PlayerConnection)con).getPlayer(), (int)BitTools.byteArrayToFloat(Xcoordinates), (int)BitTools.byteArrayToFloat(Ycoordinates));
		
		if(returnedCharacter != null) {
			((PlayerConnection)con).getPlayer().getCharacters().put(Integer.valueOf(returnedCharacter.getCharID()), returnedCharacter);
		}
		
		return ByteBuffer.wrap(createNewCharacter);
	}

	
}
