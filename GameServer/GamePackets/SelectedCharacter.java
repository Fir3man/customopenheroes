package GameServer.GamePackets;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import Connections.Connection;

public class SelectedCharacter implements Packet {

	@Override
	public void execute(ByteBuffer buff) {
		// TODO Auto-generated method stub
		
	}

	public ByteBuffer returnWritableByteBuffer(ByteBuffer buffyTheVampireSlayer, Connection con) {
		byte[] first = new byte[1452];
		byte[] second = new byte[1452];
		byte[] third = new byte[1452];
		byte[] fourth = new byte[1452];
		
		for(int i=0;i<1452;i++) {
			first[i] = 0x00;
			second[i] = 0x00;
			third[i] = 0x00;
			fourth[i] = 0x00;
		}
		
		byte[] stuffLOL = new byte[] {(byte)0xc0, (byte)0x16, (byte)0x00, (byte)0x00, (byte)0x04, (byte)0x00, (byte)0x01, (byte)0x00, (byte)0x01, (byte)0x00, (byte)0x00, (byte)0x00,  
			(byte)0x00, (byte)0x00, (byte)0x00, (byte)0x01,  
			(byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x01, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x83, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x09, (byte)0x05, (byte)0x06};
		
		byte[] cha = new byte[] {
				(byte)0x0c, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x9d, (byte)0x0f, (byte)0xbf, 
				(byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x50, (byte)0x2e
			};
		
		byte[] coordinates = new byte[] {
				(byte)0x00, (byte)0x80, (byte)0xcf, (byte)0xc4,  
				(byte)0x00, (byte)0x80, (byte)0x12, (byte)0x45   
		};
		
		for(int i=0;i<stuffLOL.length;i++) {
			first[i] = stuffLOL[i];
		}
		
		for(int i=0;i<coordinates.length;i++) {
			fourth[i+(1452-coordinates.length)] = coordinates[i];
		}
		
		SocketChannel sc = con.getChan();
		try {
			sc.write(ByteBuffer.wrap(first));
			sc.write(ByteBuffer.wrap(second));
			sc.write(ByteBuffer.wrap(third));
			sc.write(ByteBuffer.wrap(fourth));
			sc.write(ByteBuffer.wrap(cha));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	
}
