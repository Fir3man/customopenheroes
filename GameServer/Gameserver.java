package GameServer;

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import Connections.Connection;
import Core.PacketHandler.PacketHandler;
import Core.Server.ServerFacade;
import Core.Server.Core.ServerCore;
import Database.AccountDAO;
import Database.CharacterDAO;
import GameServer.GamePackets.*;
import Player.Player;
import Player.PlayerConnection;
import Player.Character;



public class Gameserver implements PacketHandler {

	private Map<Integer, Packet> packetsByHeader = new HashMap<Integer, Packet>();
	private ServerFacade sf;

	public void initialize(ServerFacade sf) {
 		this.sf = sf;
		this.packetsByHeader.put(Integer.valueOf(672), new CreateNewCharacter()); //handle character creation requests
		this.packetsByHeader.put(Integer.valueOf(1342), new Ping()); //Respond to client ping
		this.packetsByHeader.put(Integer.valueOf(675), new SelectedCharacter()); //Respond to selected character logging in game world
		this.packetsByHeader.put(Integer.valueOf(666), new Quit()); //client quits the game(clean quit)
	}


	public void processPacket(ByteBuffer buf, SocketChannel chan) {
	}

	@Override
	public void newConnection(SocketChannel chan) {
		// TODO Auto-generated method stub
		
	}


	public void newConnection(Connection con) {
		/*
		 *  Handle all your authentication logic in this block
		 */
		if(con.getIp().equals("127.0.0.1")) { //in case it's localhost - default to localhost account
				SocketChannel currentChan = con.getChan();
				this.sf.getConnections().replace(currentChan, new PlayerConnection(currentChan, 100, 100)); //upon succesful authentication - Connection becomes a PlayerConnection
				PlayerConnection plc = (PlayerConnection)this.sf.getConnections().get(currentChan);
				Player tmpl = AccountDAO.authenticate("localhost", "localhost"); //look for account entry with localhost:localhost as username:password
				if(tmpl != null) {
					plc.setPlayer(tmpl);
					plc.addWrite(ByteBuffer.wrap(Login.authSuccess));
					ArrayList<Character> characters = CharacterDAO.loadCharacters(tmpl.getAccountID());
					System.out.println("Accounts ID: " + tmpl.getAccountID());
					if(characters != null) {
						System.out.println("Account has: " + characters.size() + " character(s) in it");
						Iterator<Character> citer = characters.iterator();
						while(citer.hasNext()) {
							Character ctmp = citer.next();
							System.out.println("Char name: " + ctmp.getName());
							ctmp.initCharPacket();
							plc.addWrite(ByteBuffer.wrap(ctmp.getCharacterDataPacket()));
						}
						plc.addWrite(ByteBuffer.wrap(Login.account));
					} else {
						System.out.println("Account is empty");
						plc.addWrite(ByteBuffer.wrap(Login.account));
					}
				} else {
					con.addWrite(ByteBuffer.wrap(Login.authFail));
					ServerCore.finalizeConnection(con.getChan());
				}
		} else { //in any other case assume failed authentication
			con.addWrite(ByteBuffer.wrap(Login.authFail));
			ServerCore.finalizeConnection(con.getChan());
		}
	}


	@Override
	public ByteBuffer processPacket(ByteBuffer boss) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void processPacket(ByteBuffer buf, Connection con) {
		// TODO Auto-generated method stub
		
	}


	
	public void processList(Connection con) {
		LinkedBlockingQueue<ByteBuffer> tmpQQ = con.getReadBuffer();
		synchronized(tmpQQ) {
			while(!tmpQQ.isEmpty()) {
				ByteBuffer buf = tmpQQ.poll();
				int header = (int)((buf.get(4) & 0xFF)*666) + (int)(buf.get(6) & 0xFF); //get packet type from header (multiplying by 666 just cause we can :)
				if(this.packetsByHeader.containsKey(Integer.valueOf(header))) {
					ByteBuffer tmp = this.packetsByHeader.get(Integer.valueOf(header)).returnWritableByteBuffer(buf, con);
					if(!tmp.equals(null)) {	
						con.addWrite(tmp);
					}
				}
			}
		}
	}

}
