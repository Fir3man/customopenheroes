package Lobby;

import java.net.InetSocketAddress;

import Configuration.Configuration;
import Configuration.ConfigurationManager;
import Core.Server.ServerFacade;

public class StartLobby {
	public static void main(String[] args) {
		ConfigurationManager.init("server.xml", false);
		Configuration conf = ConfigurationManager.getConf("Lobby");
		new ServerFacade(new InetSocketAddress(conf.getIntVar("lobbyPort")), 100, 300, new Lobby()); //Client first connects to port 10000 which is lobby server
	}
}
