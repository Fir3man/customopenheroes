package Lobby;

import java.net.InetSocketAddress;

import Configuration.Configuration;
import Configuration.ConfigurationManager;
import Core.Server.ServerFacade;

public class StartCDP {
	public static void main(String[] args) {
		ConfigurationManager.init("server.xml", false);
		Configuration conf = ConfigurationManager.getConf("Lobby");
		
		new ServerFacade(new InetSocketAddress(conf.getIntVar("cdpPort")), 100, 300, new ConnectionDispatcher()); 
	}
}
