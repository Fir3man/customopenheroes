package Player;


import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import World.Grid;
import World.Location;
import World.WMap;
import World.Area;

public class Character implements Location {
	public static final int factionNeutral = 1;
	public static final int factionLawful = 2;
	public static final int factionEvil = 3;
	private String name = "NOP";
	private int level;
	private int[] stats;
	private int statPoints;
	private int skillPoints;
	private int characterClass;
	private float x,y;
	private int faction;
	private int attack, defence, hp, mana, stamina;
	private int charID;
	private int currentMap;
	private int [] areaCoords;
	private Area area;
	private Grid grid;
	private byte[] characterDataPacket;
	private Map<Integer, Boolean> iniPackets = Collections.synchronizedMap(new HashMap<Integer, Boolean>());
	
	private Player pl;
	
	
	
	
	public Character(Player pl) {
		this.pl = pl;
	}

	public Character() {
		//TODO: durp durp
	}
	
	/*
	 * Handle all logic required when character is selected in selection screen
	 * and player enters the game
	 */
	public void joinGameWorld() {
		WMap.addCharacter(this);
		this.grid = WMap.getGrid(0);
		this.area = this.grid.getArea(new int[] {0,0});
		this.area.addMember(this);
	}
	
	/*
	 * Quite the opposite of joining the game world
	 */
	public void leaveGameWorld() {
		this.area.rmMember(this.charID);
		WMap.rmCharacter(charID);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int[] getStats() {
		return stats;
	}

	public void setStats(int[] stats) {
		this.stats = stats;
	}

	public int getStatPoints() {
		return statPoints;
	}

	public void setStatPoints(int statPoints) {
		this.statPoints = statPoints;
	}

	public int getSkillPoints() {
		return skillPoints;
	}

	public void setSkillPoints(int skillPoints) {
		this.skillPoints = skillPoints;
	}

	public int getCharacterClass() {
		return characterClass;
	}

	public void setCharacterClass(int characterClass) {
		this.characterClass = characterClass;
	}

	public int getFaction() {
		return faction;
	}

	public void setFaction(int faction) {
		this.faction = faction;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public int getDefence() {
		return defence;
	}

	public void setDefence(int defence) {
		this.defence = defence;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getMana() {
		return mana;
	}

	public void setMana(int mana) {
		this.mana = mana;
	}

	public int getStamina() {
		return stamina;
	}

	public void setStamina(int stamina) {
		this.stamina = stamina;
	}

	public Player getPlayer() {
		return pl;
	}

	public void setPlayer(Player pl) {
		this.pl = pl;
	}

	public Area getArea() {
		return area;
	}

	public Grid getGrid() {
		return grid;
	}

	public int[] getAreaCoords() {
		return areaCoords;
	}

	@Override
	public int getuid() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setuid(int uid) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public float getlastknownX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getlastknownY() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public SocketChannel GetChannel() {
		return this.pl.getSc();
	}

	@Override
	public short getState() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getCharID() {
		return charID;
	}

	public void setCharID(int charID) {
		this.charID = charID;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public int getCurrentMap() {
		return currentMap;
	}

	public void setCurrentMap(int currentMap) {
		this.currentMap = currentMap;
	}

	public byte[] getCharacterDataPacket() {
		return characterDataPacket;
	}

	public void setCharacterDataPacket(byte[] characterDataPacket) {
		this.characterDataPacket = characterDataPacket;
	}
	
	/*
	 * Holy mother of a monster packet.
	 * Format a character data packet for this character based on it's attributes.
	 */
	public void initCharPacket() {
		byte[] cdata = new byte[660];
		byte[] charname = this.getName().getBytes();
		int[] stats = this.getStats();
		int skills = this.getSkillPoints();
		int statp = this.getStatPoints();
		int hp = this.getHp();
		int mana = this.getMana();
		int stamina = this.getStamina();

		for(int i=0;i<charname.length;i++) {
			cdata[i+11] = charname[i];
		}
		if(this.getCharacterClass() == 2) {
			cdata[51] = 0x02;	//sin has gender byte(2 = female(yeah.. don't ask me why CRS add gender byte in their packet))
			cdata[59] = 0x02;	//sin class is 2
		} else {
		    cdata[51] = 0x01;
			cdata[59] = (byte)this.getCharacterClass(); //in other case only class byte will change
		}
		cdata[65] = (byte)this.getLevel();
	
        cdata[583] = (byte)stats[0]; //stat 1
		cdata[584] = (byte)0x00;	 //stat 1, stats are divided between 2 bytes, first one are the stats, its like FF 00 = 255, 0F 00=15, 10 F0=271
		cdata[585] = (byte)stats[1]; //stat 2
		cdata[586] = (byte)0x00;	 //stat 2
		cdata[587] = (byte)stats[2]; //stat 3
		cdata[588] = (byte)0x00;	 //stat 3
		cdata[589] = (byte)stats[3]; //stat 4
		cdata[590] = (byte)0x00;	 //stat 4
		cdata[591] = (byte)stats[4]; //stat 5
		cdata[592] = (byte)0x00;	 //stat 5
		cdata[593] = (byte)statp;	 //no explination yet but think it's statspoints left
		
		cdata[613] = (byte)skills; //skillpoints aren't retrieved right atm       
		cdata[614] = (byte)0x00;   // 613 and 614 are the bytes for skillpoints
		
	    cdata[0] = (byte)0x94;
		cdata[1] = (byte)0x02;
		cdata[4] = (byte)0x03;
		cdata[6] = (byte)0x04;
		
		for(int i=0;i<3;i++) { cdata[i+8] = (byte)0x01; }
		for(int i=0;i<16;i++) { cdata[i+28] = (byte)0x30; }
		
		cdata[45] = (byte)0x01;
		cdata[53] = (byte)0x01;
		cdata[55] = (byte)0x01; //standard its 01 but 00 gives no errors, no explination yet
		cdata[63] = (byte)0x02; //standard its 02 but 00 goes as well nothing changes though, no explination yet
			
		byte[] stuff = new byte[] { (byte)0x00, (byte)0xa0, (byte)0x01, (byte)0x00, (byte)0x00, (byte)0x7c, (byte)0x01, (byte)0x00,				  
			(byte)0x00, (byte)0x01, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0xf3,
			(byte)0xd4, (byte)0x13, (byte)0xc5, (byte)0x9a, (byte)0xb9, (byte)0xbd, (byte)0xc5, (byte)0x00, (byte)0x00, (byte)0x02, (byte)0x00 }; // something to do with co�rdinates
		
		for(int i=0;i<stuff.length;i++) {
			cdata[i+66] = stuff[i];

		cdata[67] = (byte)hp;		//current hp, not working properly
		cdata[71] = (byte)mana;		//current mana, not working properly
		cdata[74] = (byte)stamina;  //probably stamina but not confirmed yet, not working properly
		
		}
		
		this.setCharacterDataPacket(cdata);
	}




	// send packet buf to all nearby players
	private void sendToMap(ByteBuffer buf) {
		int [] area = this.getAreaCoords();
		
		area[0] -=1;
		area[1] -=1;
		Area tmp = null;
		for (int a=0; a <3; a++){
			for (int b=0; b <3; b++){
				if (this.getGrid().areaExists(area)){
					tmp = this.getGrid().getArea(area);
					tmp.sendToMembers(this.getuid(), buf);
					area[1]++;
				}
			}
			area[0]++;
		}
		
	}

	@Override
	public ByteBuffer getInitPacket() {
		// TODO Auto-generated method stub
		return null;
	}
	private void setInitStatus(int uid, boolean b){
		if (b) this.iniPackets.put(uid, b);
		else if (this.hasInit(uid)) this.iniPackets.remove(uid);
	}
	private boolean hasInit(int uid){
		return this.iniPackets.containsKey(uid);
	}
	
	
}
