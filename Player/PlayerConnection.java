package Player;

import java.nio.channels.SocketChannel;

import Connections.Connection;

/**
 * TODO: Utilize this class as special occurance of Connection
 */

public class PlayerConnection extends Connection {
	private Player pl;
	
	
	public PlayerConnection(SocketChannel sc, int rsize, int wsize) {
		super(sc, rsize, wsize);
		// TODO Auto-generated constructor stub
	}

	public Player getPlayer() {
		return this.pl;
	}

	public void setPlayer(Player pl) {
		this.pl = pl;
	}
	

}
