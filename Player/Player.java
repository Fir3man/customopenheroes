package Player;

import java.nio.channels.SocketChannel;
import java.util.Map;

public class Player {
	private Map<Integer, Character> characters;
	private int accountID;
	private int flag;
	private SocketChannel sc;
	
	public Player(int id) {
		this.accountID = id;
	}
	
	public Map<Integer, Character> getCharacters() {
		return characters;
	}
	public void setCharacters(Map<Integer, Character> characters) {
		this.characters = characters;
	}
	public int getAccountID() {
		return accountID;
	}
	public void setAccountID(int accountID) {
		this.accountID = accountID;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}

	public SocketChannel getSc() {
		return sc;
	}
	
}
