package Mob;

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import World.Area;
import World.Location;
import World.WMap;


/*
 *  Mob.class
 *  Provides basic mob logic functions 
 */
public class Mob implements Location{
	
	
	private int mobID, uid;
	private MobData data;
	private MobController control;
	
	/*
	 * Initializes the mob
	 * Params:
	 * mobID = type of mob in question
	 * id = unique ID of mob
	 * mdata = pointer to mobs data object 
	 */
	
	public Mob(int mobID, int id, MobData mdata, MobController cont) {
		super();
		this.uid = id;
		this.mobID = mobID;
		this.data = mdata;
		this.data.setPath (  this.createPath(this.data.getWaypointCount() , this.data.getWaypointHop(), data.getSpawnx(), data.getSpawny()));
		this.data.setCurentWaypoint(0);
		this.control = cont;
		this.joinGrid(this.data.getGridID());
	}
	
	public int getMobID() {
		return mobID;
	}

	@Override
	public int getuid() {
		// TODO Auto-generated method stub
		return this.uid;
	}
	@Override
	public void setuid(int uid) {
		// TODO Auto-generated method stub
		this.uid = uid;
		
	}
	@Override
	public float getlastknownX() {
		// TODO Auto-generated method stub
		return this.data.getX();
	}
	@Override
	public float getlastknownY() {
		// TODO Auto-generated method stub
		return this.data.getY();
	}
	@Override
	public SocketChannel GetChannel() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public short getState() {
		// TODO Auto-generated method stub
		return 0;
	}
	// Join mob into the grid based proximity system 
	public void joinGrid(int grid)
	{
	    this.data.setGrid(new int []{146, 233});
	    this.data.setMyGrid(WMap.getGrid(grid));
	    
	    if (this.data.hasGrid()){ System.out.println("FUUUUUUUUUuuuuuuuuuuu!!!!!"); }
	    if(this.data.areaExists(this.data.getGrid()))
	    { 
	      this.data.setMyArea(this.data.getMyGrid().getArea(this.data.getGrid()));
	      
		  this.data.addAreaMember(this);
	    }
	    else { System.out.println("wth its no workin");}
		// WMap.AddMob(this.uid, this);
		this.spawnage();

	}
	// perform actions needed to spawn the mob in the game world
	private void spawnage() {
		// TODO Auto-generated method stub
		
	}
	
	// return the waypoint number i
	public float[] getWaypoint(int i)
	{
	    return (float [])this.data.getWaypoint(i);
	}
	// return total ammount of waypoints generated for the mob
	public int waypointCount()
	{
	    return this.data.getPathSize();
	}
	/* generates the waypoint needed for the idle moving pattern
	 * Params:
	 * wpc = waypoints per each side
	 * hop = distance between waypoints
	 * x = starting x coordinate
	 * y = starting y coordinate
	 */
	
	public Map<Integer, Object> createPath(int wpc, float hop, float x, float y)
	{
	    Random generator = new Random();
	    Map<Integer, Object> path = Collections.synchronizedMap(new HashMap<Integer, Object>());
	    int rand = generator.nextInt(100);	
		float tmp[] = new float[]{x, y};
		float []pfft;
		// int counter = 0;
		int side = wpc;
		for (int a=0; a < wpc; a++)
		{
		  if ((rand % 2) == 0){ tmp[0] += hop;    }
		  else { tmp[0] -= hop; }
		  pfft = new float[]{tmp[0], tmp[1] };
		  path.put(a, pfft);
		  // System.out.println("["+a+"] X:" + pfft[0] + " Y:" + pfft[1]);
		}
		for (int b=wpc; b < (wpc+side); b++)
		{
		  if ((rand % 2) == 0){ tmp[1] += hop;    }
		  else { tmp[1] -= hop; }
		  pfft = new float[]{tmp[0], tmp[1] };
		  path.put(b, pfft);
		  // System.out.println("["+b+"] X:" + pfft[0] + " Y:" + pfft[1]);
		}
		wpc += side;
		for (int c=wpc; c < (wpc+side); c++)
		{
		  if ((rand % 2) == 0){ tmp[0] -= hop;  }
		  else { tmp[0] += hop;   }
		  pfft = new float[]{tmp[0], tmp[1] };
		  path.put(c, pfft);
		  // System.out.println("["+c+"] X:" + pfft[0] + " Y:" + pfft[1]);
		}
		wpc += side;
		for (int d=wpc; d < (wpc+side); d++)
		{
		  if ((rand % 2) == 0){ tmp[1] -= hop; }
		  else { tmp[1] += hop;  }
		  pfft = new float[]{tmp[0], tmp[1] };
		  path.put(d, pfft);
		  // System.out.println("["+d+"] X:" + pfft[0] + " Y:" + pfft[1]);
		}
		return path;
	  }
	// perform 1 action on the map based on the status of mob
	public boolean run() {
		boolean hasPlayers = false;
		int i = this.data.getCurentWaypoint();
		float  []waypoint = new float[]{0,0};
		// int hiDmg, hiID;
		
		// check if mob is alive
		if (this.data.isAlive()){
			// check if mob has gone too far from its initial spawn point
			if (WMap.distance(this.data.getX(), this.data.getY(), this.data.getSpawny(), this.data.getSpawny()) > this.data.getMoveRange()){
				this.reset(true);
			}
			// logic if mob has been aggroed
			else if(this.data.isAggro()){
				
				if (WMap.CharacterExists(this.data.getAggroID())){
						Location loc = WMap.getCharacter(this.data.getAggroID());
						// attack target and/or move towards it
						if (WMap.distance(this.data.getX(), this.data.getY(), loc.getlastknownX(), loc.getlastknownY()) < this.data.getAttackRange()){
							this.attack(loc);
						}
						this.setLocation(loc.getlastknownX(), loc.getlastknownY());
				}
				else {
					this.reset(true);
				}	
			}
			// mob hasn't been aggroed
			else {
				// move mob to it's next waypoint
				if (i >= this.waypointCount()) i=0;
				   
			    waypoint = this.getWaypoint(i);
			    this.setLocation(waypoint[0], waypoint[1]);
				i++;
				this.data.setCurentWaypoint(i);
				
				// check if there are mobs within aggro range of the mob
				int [] area = this.data.getGrid();
				area[0] -=1;
				area[1] -=1;
				Area tmp = null;
				
				for (int a=0; a <3; a++){
					for (int b=0; b <3; b++){
						if (this.data.getMyGrid().areaExists(area)){
							tmp = this.data.getMyGrid().getArea(area);
							if (tmp.hasPlayers()){
								this.checkAggro(tmp);
								hasPlayers = true;
							}
							area[1]++;
						}
					}
					area[0]++;
				}
			
				
			}
			
		}
		return hasPlayers;
		
	}
	// attack target loc
	private void attack(Location loc) {
		
	}

	// resets mobs data
	private void reset(boolean sendMove) {
		this.data.resetDamage();
		this.data.setHp(this.data.getMaxhp());
		this.data.setCurentWaypoint(0);
		this.data.setX(this.data.getSpawnx());
		this.data.setY(this.data.getSpawny());
		if (sendMove) this.send(MobPackets.getMovePacket(this.mobID, this.uid, this.data.getX(), this.data.getY()));
		this.data.setAggro(false);
		this.data.setAggroID(0);
		
	}
	public void recDamge(int uid, int dmg){
		if (this.data.hasPlayerDamage(uid)){
			int tmp = this.data.getPlayerDamage(uid);
			tmp += dmg;
			this.data.setDamage(uid, tmp);
		}
		else{
			this.data.setDamage(uid, dmg);
		}
		
		Map <Integer, Integer>mp = this.data.getDamage();
		synchronized(mp) {  //synchronized iteration for thread safe operations
			Iterator <Map.Entry<Integer, Integer>> it = mp.entrySet().iterator();
			int key;
			int value = 0;
			int hiDmg = 0;
			int hiID = 0;
			while (it.hasNext()) {
				Map.Entry<Integer, Integer> pairs = it.next();
				key = pairs.getKey();
				value = pairs.getValue();
				if (value > hiDmg){
					hiDmg = value;
					hiID = key;
				}
			}
			this.data.setAggro(true);
			this.data.setAggroID(hiID);
		}		

		this.data.reduceHp(dmg);
		if (this.data.getHp() <= 0) this.die();
	  }
	// perform actions needed to finalize mob's death
	private void die(){
		this.data.setDied(System.currentTimeMillis());
		this.data.setAlive(false);
		this.send(MobPackets.getDeathPacket());
		this.reset(false);
	}
	// check if mob is close enough to player to aggro it
	private void checkAggro(Area area) {
		Map<Integer, Location> mp = area.getMemberMap();
		Location loc;
		synchronized(mp)
		{
			  Iterator<Map.Entry<Integer, Location>> iter = mp.entrySet().iterator();
			  while(iter.hasNext()) {
				  Map.Entry<Integer, Location> pairs = iter.next();
				  loc = pairs.getValue();
				  
				  if (loc.GetChannel() != null){
					  if (WMap.distance(this.data.getX(), this.data.getY(), loc.getlastknownX(), loc.getlastknownY()) < this.data.getAggroRange()){
						  this.data.setAggro(true);
						  this.data.setAggroID(loc.getuid());
					  }
					 
				  }
			  }
			  
		}
		
	}

	// send packet to all nearby players
	private void send(ByteBuffer buf) {
		int [] area = this.data.getGrid();
		
		area[0] -=1;
		area[1] -=1;
		Area tmp = null;
		for (int a=0; a <3; a++){
			for (int b=0; b <3; b++){
				if (this.data.getMyGrid().areaExists(area)){
					tmp = this.data.getMyGrid().getArea(area);
					tmp.sendToMembers(this.getuid(), buf);
					area[1]++;
				}
			}
			area[0]++;
		}
		
	}
	// set mobs location on the map and send move packet to players
	private void setLocation(float x, float y) {
		int [] area = this.data.getGrid();
		int [] tmp;
		this.data.setX(x);
		this.data.setY(y);
		tmp = this.data.getMyArea().update(this.uid);
		if (tmp[0] != area[0] || tmp[1] != area[1]){
			this.data.getMyArea().rmMember(uid);
			this.data.setGrid(tmp);
			this.data.setMyArea(this.data.getMyGrid().getArea(tmp));
			this.data.getMyArea().addMember(this);
			
		}
		ByteBuffer buf = MobPackets.getMovePacket(this.mobID, this.uid, this.data.getX(), this.data.getY());
		this.send(buf);
	}
	// return reference to this mob's controller
	public MobController getControl() {
		return control;
	}
	public ByteBuffer getInitPacket(){
		return MobPackets.getInitialPacket(this.mobID, this.uid, this.data.getX(), this.data.getY());
	}
	
	

}
