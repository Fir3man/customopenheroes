package Mob;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import Database.MobDAO;

public class MobController implements Runnable {
	private Map<Integer, Mob> mobs = Collections.synchronizedMap(new HashMap<Integer, Mob>());
	private int mobID, mobCount, uidPool;
	private boolean active;
	
	
	public MobController(int mobID, int mobCount, int uidPool) {
		super();
		this.mobID = mobID;
		this.mobCount = mobCount;
		this.uidPool = uidPool;
		this.setActive(false);
		this.init();
	}
	private void init(){
		MobData template = MobDAO.getMobData(mobID);
		MobData tmp= null;
		Mob mob = null;
		int uid = uidPool;
		for (int i=0; i < this.mobCount; i++)
		{
			try {
				tmp = template.clone();
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mob = new Mob(this.mobID, uid, tmp, this);
			uid++;
			mobs.put(i, mob);
			
		}
		
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public void run(){
		this.setActive(true);
		boolean hasPlayers;
		
		while(this.active){
			hasPlayers = false;
			synchronized(this.mobs){
				Iterator<Map.Entry<Integer, Mob>> iter = this.mobs.entrySet().iterator();
				  while(iter.hasNext()) {
					  Map.Entry<Integer, Mob> pairs = iter.next();
					  hasPlayers = pairs.getValue().run();
				  }
			}
			try {
				if (!hasPlayers) this.setActive(false);
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	

}
