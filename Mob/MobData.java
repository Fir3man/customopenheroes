package Mob;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import World.Area;
import World.Grid;

/*
 * MobData.class
 * Stores all mobs data 
 */

public class MobData implements Cloneable{
	private int lvl, attack, defence, hp, maxhp, basexp;
	private boolean alive, active, aggro;
	private float x,y, spawnx,spawny;
	private int aggroRange = 30;
	private int followRange = 200;
	private int moveRange = 500;
	private int attackRange = 5;
	private long respawnTime = 10000;
	private Grid myGrid;
	private Area myArea;
	private int [] grid;
	private int mobID, uid, aggroID;
	private int curentWaypoint, gridID;
	private int waypointCount, waypointHop;
	private long died;
	private Map<Integer, Integer> damage = Collections.synchronizedMap(new HashMap<Integer, Integer>());
	private Map<Integer, Object> Path = Collections.synchronizedMap(new HashMap<Integer, Object>());
	
	public int getLvl() {
		return lvl;
	}
	public void setLvl(int lvl) {
		this.lvl = lvl;
	}
	public int getAttack() {
		return attack;
	}
	public void setAttack(int attack) {
		this.attack = attack;
	}
	public int getDefence() {
		return defence;
	}
	public void setDefence(int defence) {
		this.defence = defence;
	}
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getBasexp() {
		return basexp;
	}
	public void setBasexp(int basexp) {
		this.basexp = basexp;
	}
	public boolean isAlive() {
		return alive;
	}
	public void setAlive(boolean alive) {
		this.alive = alive;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	public float getSpawnx() {
		return spawnx;
	}
	public void setSpawnx(float spawnx) {
		this.spawnx = spawnx;
	}
	public float getSpawny() {
		return spawny;
	}
	public void setSpawny(float spawny) {
		this.spawny = spawny;
	}
	public int getAggroRange() {
		return aggroRange;
	}
	public void setAggroRange(int aggroRange) {
		this.aggroRange = aggroRange;
	}
	public int getFollowRange() {
		return followRange;
	}
	public void setFollowRange(int followRange) {
		this.followRange = followRange;
	}
	public int getMoveRange() {
		return moveRange;
	}
	public void setMoveRange(int moveRange) {
		this.moveRange = moveRange;
	}
	public long getRespawnTime() {
		return respawnTime;
	}
	public void setRespawnTime(long respawnTime) {
		this.respawnTime = respawnTime;
	}
	public Grid getMyGrid() {
		return myGrid;
	}
	public void setMyGrid(Grid myGrid) {
		this.myGrid = myGrid;
	}
	public Area getMyArea() {
		return myArea;
	}
	public void setMyArea(Area myArea) {
		this.myArea = myArea;
	}
	public int[] getGrid() {
		return grid;
	}
	public void setGrid(int[] grid) {
		this.grid = grid;
	}
	public int getMobID() {
		return mobID;
	}
	public void setMobID(int mobID) {
		this.mobID = mobID;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public Map<Integer, Integer> getDamage() {
		return damage;
	}
	public void setDamage(int uid, int dmg) {
		this.damage.put(uid, dmg);
	}
	public boolean hasDamage(){
		return !this.damage.isEmpty();
	}
	public void resetDamage(){
		this.damage.clear();
	}
	public boolean hasPlayerDamage(int uid){
		return this.damage.containsKey(uid);
	}
	public Map<Integer, Object> getPath() {
		return Path;
	}
	public void setPath(Map<Integer, Object> path) {
		Path = path;
	}
	public boolean hasGrid() {
		// TODO Auto-generated method stub
		if (this.myGrid != null) return true;
		return false;
	}
	public boolean areaExists(int []grid)
	{
		return this.myGrid.areaExists(grid);
		
	}
	public float[] getWaypoint(int i) {
		// TODO Auto-generated method stub
		return (float[]) this.Path.get(i);
	}
	public int getPathSize() {
		// TODO Auto-generated method stub
		return this.Path.size();
	}
	public void addAreaMember(Mob mob) {
		// TODO Auto-generated method stub
		this.myArea.addMember(mob);
		
	}
	public int getCurentWaypoint() {
		return curentWaypoint;
	}
	public void setCurentWaypoint(int curentWaypoint) {
		this.curentWaypoint = curentWaypoint;
	}
	public int getGridID() {
		return gridID;
	}
	public void setGridID(int gridID) {
		this.gridID = gridID;
	}
	public boolean isAggro() {
		return aggro;
	}
	public void setAggro(boolean aggro) {
		this.aggro = aggro;
	}
	public int getAggroID() {
		return aggroID;
	}
	public void setAggroID(int aggroID) {
		this.aggroID = aggroID;
	}
	public int getMaxhp() {
		return maxhp;
	}
	public void setMaxhp(int maxhp) {
		this.maxhp = maxhp;
	}
	public long getDied() {
		return died;
	}
	public void setDied(long died) {
		this.died = died;
	}
	protected MobData clone() throws CloneNotSupportedException {
        return (MobData)super.clone();
    }
	public int getPlayerDamage(int uid) {
		return this.damage.get(uid);
	}
	public void reduceHp(int dmg) {
		this.hp -= dmg;
	}
	public int getWaypointHop() {
		return waypointHop;
	}
	public void setWaypointHop(int waypointHop) {
		this.waypointHop = waypointHop;
	}
	public int getWaypointCount() {
		return waypointCount;
	}
	public void setWaypointCount(int waypointCount) {
		this.waypointCount = waypointCount;
	}
	public int getAttackRange() {
		return attackRange;
	}
	public void setAttackRange(int attackRange) {
		this.attackRange = attackRange;
	}


}
