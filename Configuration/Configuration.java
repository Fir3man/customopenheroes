package Configuration;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class Configuration {
	private volatile ConcurrentMap<String, String> variables;
	  
	public Configuration(){
		this.variables = new ConcurrentHashMap<String, String>();
	}
	public String getVar(String var){
		return this.variables.get(var);
	}
	public void setVar(String var, String val){
		this.variables.putIfAbsent(var, val);
	}
	public int getIntVar(String var) {
		return Integer.parseInt(this.variables.get(var));
	}

}
