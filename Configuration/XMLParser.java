package Configuration;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLParser {
	

	public static Element getRoot(String filename){
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Document dom = null;
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			dom = db.parse(filename);
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch(SAXException se) {
			se.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		Element root = dom.getDocumentElement();
		return root;
		
	}
	public static String getTextValue(Element el, String tag){
		String val = null;
		NodeList nl = el.getElementsByTagName(tag);
		if (nl != null && nl.getLength() > 0){
			Element t = (Element)nl.item(0);
			val = t.getFirstChild().getNodeValue();
		}
		return val;
	}
	public static Element getFirstElement(Element el, String tag){
		return XMLParser.getElement(el, tag, 0);
	}	
	public static Element getElement(Element el, String tag, int n){
		Element t = null;
		NodeList nl = el.getElementsByTagName(tag);
		if (nl != null && nl.getLength() > 0){
			 t = (Element)nl.item(n);
		}
		return t;
	}
	public static NodeList getNodeList(Element el, String tag){
		return el.getElementsByTagName(tag);
	}
	public static int getNodeCount(Element el, String tag){
		NodeList nl = el.getElementsByTagName(tag);
		return nl.getLength();
	}
	public static String getElementName(Element el){
		return el.getNodeName();
	}
	public static int getIntValue(Element el, String tag) {
		return Integer.parseInt(XMLParser.getTextValue(el, tag));
	}
	public static String getAttrValue(Element el, String attr){
		return el.getAttribute(attr);
	}
	public static int getAttrIntValue(Element el, String attr){
		return Integer.parseInt(el.getAttribute(attr));
	}
}
