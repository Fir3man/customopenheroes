package Configuration;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Element;



public class ConfigurationManager {
	private static final Map<String, Configuration> conf = Collections.synchronizedMap(new HashMap<String, Configuration>());

	public static void initAll(String confFile) {
		conf.put("GameServer", ConfigurationManager.parseServerConf(confFile));
		conf.put("Lobby", ConfigurationManager.parseLobbyConf(confFile));
	}
	public static void init(String confFile, boolean gameserver){
		if (gameserver) conf.put("GameServer", ConfigurationManager.parseServerConf(confFile));
		else conf.put("Lobby", ConfigurationManager.parseLobbyConf(confFile));
	}
	public static Configuration getConf(String name) {
		return conf.get(name);
	}
	public static Configuration parseServerConf(String filename)
	{
		Configuration conf = new Configuration();
	
		Element root = XMLParser.getRoot(filename);
		if (XMLParser.getElementName(root) == "GameServer")
		{
			Element el = XMLParser.getFirstElement(root, "database");
			if (el != null){
				conf.setVar("db", XMLParser.getTextValue(el, "db"));
				conf.setVar("host", XMLParser.getTextValue(el, "host"));
				conf.setVar("username", XMLParser.getTextValue(el, "username"));
				conf.setVar("password", XMLParser.getTextValue(el, "password"));
				conf.setVar("driver", XMLParser.getTextValue(el, "driver"));
			}
			else { System.out.println("Failed to load dabatase info from configuration file"); }
			el = XMLParser.getFirstElement(root, "server");
			if (el != null){
				conf.setVar("port", XMLParser.getTextValue(el, "port"));
			}
			else { System.out.println("Failed to load server info from configuration file"); }
		}
		return conf;	
	}
	public static Configuration parseLobbyConf(String filename){
		Configuration conf = new Configuration();
		
		Element root = XMLParser.getRoot(filename);
		if (XMLParser.getElementName(root) == "GameServer")
		{
			Element el = XMLParser.getFirstElement(root, "lobby");
			if (el != null){
				conf.setVar("lobbyPort", XMLParser.getTextValue(el, "lobbyPort"));
				conf.setVar("cdpPort", XMLParser.getTextValue(el, "cdpPort"));
			}
		}
		return conf;
	}

}
